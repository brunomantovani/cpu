// ConsoleApplicationTeste.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <conio.h>
#include <stdbool.h>

typedef struct Processo {
	int identificadorDoProcesso;
	int tempoCriacao;
	int duracaoMaxima;
	int prioridade;
	int tempoVida;
	int tempoTotal;
	int trocas;
	bool situacao;
}PROCESSO;

void armazenarProcesso();
void ordenaProcesso();
void imprimeProcessos(PROCESSO pro);
//int maiorPrioridade();
int proximaPrioridade();
int menorPrioridade(PROCESSO processos[]);
void calculoDePrioridade();
int proximaPrioridade(int posicaoAtual);


PROCESSO processos[10];

int main()
{
	fflush(stdin);
	armazenarProcesso();
	maiorPrioridade();
	calculoDePrioridade();	

	getch();
    return 0;
}

void armazenarProcesso() {

	for (int i = 0; i < 10; i++) {
		processos[i].identificadorDoProcesso = i+1;

		printf("Dados do processo %d \n\n", (i + 1));

		printf("Entre com o tempo de criacao: ");
		scanf("%d", &processos[i].tempoCriacao);

		printf("Entre com a duracao max: ");
		scanf("%d", &processos[i].duracaoMaxima);

		printf("Entre com a prioridade: ");
		scanf("%d", &processos[i].prioridade);

		processos[i].tempoTotal = 0;
		processos[i].tempoVida = 0;
		processos[i].trocas = 0;
		processos[i].situacao = true;
		processos[i].tempoCriacao = i;

		printf("\n");		

	}
}


void calculoDePrioridade() {
	bool verif = true;
	int processoAtual = 0;

	int j = 0;
	while (true)
	{
		while (j < 10) {
			while (processos[j].duracaoMaxima != 0) {

				if (processos[j].situacao == true) {
					processos[j].duracaoMaxima--;
					processos[j].tempoVida++;
					processos[j].trocas++;
					if (processos[j].duracaoMaxima <= 0) {
						processos[j].situacao = false;
					}
				}
			}
			j++;
		}
		break;
		/*
		for (int i = 0; i < 10; i++) {
			if (processos[i].situacao == false) {
				break;
			}
			continue;
		}
		*/
	}
		
	for (int i = 0; i < 10; i++) {
		imprimeProcessos(processos[i]);
	}


	
}

/*
int maiorPrioridade() {
	int posicao = 0;
	int i = 0;

	for (int i = 0; i < 10; i++) {
		if (processos[i].situacao == false)
			continue;
		else if (posicao == 0){
			posicao = i;
			break;
		}
		else if (posicao > processos[i].prioridade && processos[i].duracaoMaxima > 0){
			posicao = i;
			break;
		}
	}

	/*
	while (i < 10) {
		if (processos[i].situacao == false)
			continue;
		else if (posicao == 0) {
			posicao = i;
			break;
		}
		else if (posicao > processos[i].prioridade && processos[i].duracaoMaxima > 0) {
			posicao = i;
			break;
		}
	
		i++;
	}
	return processos[posicao].prioridade;
}

*/

int proximaPrioridade(int posicaoAtual) {
	int proximaPosicao = 0;
	for (int i = 0; i < 10; i++) {
		if (processos[posicaoAtual].prioridade == processos[i].prioridade)
			continue;
		else if (processos[proximaPosicao].prioridade >= processos[i].prioridade)
			proximaPosicao = i;
	}
	return proximaPosicao;
}

void ordenaProcesso() {
	struct Processo prioridadesOrdenado[11];
	for (int i = 0; i < 10; i++) {
		int position = processos[i].prioridade;
		prioridadesOrdenado[position - 1] = processos[i];
	}

	for (int i = 0; i < 10; i++) {
		processos[i] = prioridadesOrdenado[i];
	}

}


void imprimeProcessos(PROCESSO pro) {
		
		printf("------------------------------------------------\n");
		printf("Processo %d \n", pro.identificadorDoProcesso);
		printf("Tempo de criacao: %d \n", pro.tempoCriacao);
		printf("Duracao maxima: %d \n", pro.duracaoMaxima);
		printf("Prioridade: %d \n", pro.prioridade);
		printf("Tempo Total: %d \n", pro.tempoTotal);
		printf("Tempo de Vida: %d \n", pro.tempoVida);
		printf("Trocas: %d \n", pro.trocas);
		printf("Situacao: %d \n", pro.situacao);

		printf("");
	
}

int menorPrioridade(PROCESSO processos[]) {
	int posicao = 0;
	for (int i = 0; i < 10; i++) {
		if (processos[i].situacao == false)
			continue;
		else if (posicao == 0)
			posicao = i;
		else if (posicao < processos[i].prioridade && processos[i].duracaoMaxima > 0)
			posicao = i;
	}
	return posicao;
}

int proximaPrioridade(PROCESSO processos[], int posicaoAtual) {
	int proximaPosicao = menorPrioridade(processos);
	//int prioridadeDaVez = processos[posicaoAtual].prioridade;
	for (int i = 0; i < 10; i++) {
		if (processos[i].situacao == false || posicaoAtual == i)
			continue;
		else if (processos[proximaPosicao].prioridade >= processos[i].prioridade)
			proximaPosicao = i;
	}
	return proximaPosicao;
}